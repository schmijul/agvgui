import tkinter as tk
from PIL import ImageTk, Image
import cv2
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import time 
from concurrent.futures import ThreadPoolExecutor


def update_histograms(ax1, ax2, data1, data2):
    """
    Update the histograms with new data
    
    :param ax1: The axis of the first histogram
    :param ax2: The axis of the second histogram
    :param data1: The data of the first histogram
    :param data2: The data of the second histogram
    
    :return: None
    """
    z1 = data1[:, :, 0]
    z2 = data2[:, :, 0]

    hist1, bins1 = np.histogram(z1, bins=50)
    hist2, bins2 = np.histogram(z2, bins=50)

    ax1.clear()
    ax1.bar(bins1[:-1], hist1, width=np.diff(bins1), align="edge")

    ax2.clear()
    ax2.bar(bins2[:-1], hist2, width=np.diff(bins2), align="edge")

def update_canvas_images(canvas_img1, canvas_img2, data1, data2):
    """
    Update the images in the canvas

    :param canvas_img1: The canvas of the first image
    :param canvas_img2: The canvas of the second image
    :param data1: The data of the first image
    :param data2: The data of the second image

    :return: None
    """
    img1 = Image.fromarray(np.uint8(data1))
    img2 = Image.fromarray(np.uint8(data2))

    imgtk1 = ImageTk.PhotoImage(image=img1)
    canvas_img1.imgtk = imgtk1
    canvas_img1.itemconfig(canvas_img1_img, image=imgtk1)

    imgtk2 = ImageTk.PhotoImage(image=img2)
    canvas_img2.imgtk = imgtk2
    canvas_img2.itemconfig(canvas_img2_img, image=imgtk2)

def process_frame(frame):
    """
    Process the frame and return the image

    :param frame (np.array): The frame to be processed

    :return: The processed image
    """
    frame = cv2.resize(frame, RESIZED_VIDEO_SIZE)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    return imgtk



def run_update_histograms():
    update_histograms(ax1, ax2, data1, data2)

def run_update_canvas_images():
    update_canvas_images(canvas_img1, canvas_img2, data1, data2)

def video_stream():
    _, frame = cap.read()
    if frame is None:
        root.quit()
    else:
        imgtk = process_frame(frame)
        lmain.imgtk = imgtk
        lmain.configure(image=imgtk)

        if int(cap.get(cv2.CAP_PROP_POS_FRAMES)) % int(cap.get(cv2.CAP_PROP_FPS)) == 0:
            data1[np.random.randint(0, 299, 10), np.random.randint(0, 299, 10), :] = 0
            data2[np.random.randint(0, 299, 10), np.random.randint(0, 299, 10), :] = 0

            # Run update_histograms and update_canvas_images asynchronously
            
            root.after_idle(update_histograms, ax1, ax2, data1, data2)
            root.after_idle(update_canvas_images, canvas_img1, canvas_img2, data1, data2)

            # Schedule the drawing of histograms and canvas images asynchronously
            root.after(1, canvas.draw)
            root.after(1, canvas2.draw)
        lmain.after(1, video_stream)

def create_title_label(app, title_text:str):
    """
    Create the title label

    :param app: The main frame of the GUI
    :param title_text(str): The text for the title label
    """
    title_label = tk.Label(app, text=title_text, font=("Arial", 24), bg="white")
    title_label.grid(row=0, column=1, pady=(10, 0))
    return title_label


def create_button(app,buttontext:str):
    """
    Create a button in the GUI

    :param app: The main frame of the GUI
    :param buttontext: The text of the button
    """
    button = tk.Button(app, text=buttontext, command=button_click, bg="white")
    button.grid(row=3, column=2, pady=5)
    return button

def create_timer_label(app):
    """
    Create the timer label

    :param app: The main frame of the GUI
    """
    timer_label = tk.Label(app, text="00:00", font=("Arial", 14), bg="white")
    timer_label.grid(row=2, column=1, pady=5)
    return timer_label

def update_timer(timer_label, end_time):
    """
    Update the timer label

    :param timer_label: The label to be updated
    :param end_time: The end time of the timer   
    """
    remaining_time = end_time - time.time()
    if remaining_time <= 0:
        timer_label.config(text="00:00")
        
    else:
        minutes, seconds = divmod(remaining_time, 60)
        timer_label.config(text=f"{int(minutes):02d}:{int(seconds):02d}")
        timer_label.after(1000, update_timer, timer_label, end_time)

def create_video_label(app):
    """
    Create the label for the video
    
    :param app: The main frame of the GUI
    """
    lmain = tk.Label(app)
    lmain.grid(row=1, column=1, pady=30)
    return lmain

def create_histogram_frames(app):
    """
    Create the frames for the histograms
    
    :param app: The main frame of the GUI
    """
    hist_frame1 = tk.Frame(app, bg="white")
    hist_frame1.grid(row=2, column=0, padx=5, pady=5)

    hist_frame2 = tk.Frame(app, bg="white")
    hist_frame2.grid(row=2, column=2, padx=5, pady=5)

    fig1 = Figure(figsize=(2, 2), dpi=100)
    ax1 = fig1.add_subplot(111)
    canvas = FigureCanvasTkAgg(fig1, master=hist_frame1)
    canvas.get_tk_widget().grid(row=2, column=0, padx=5, pady=5)

    fig2 = Figure(figsize=(2, 2), dpi=100)
    ax2 = fig2.add_subplot(111)
    ax2.set_title("Player 2 Data Distribution")
    canvas2 = FigureCanvasTkAgg(fig2, master=hist_frame2)
    canvas2.get_tk_widget().grid(row=2, column=0, padx=5, pady=5)

    return hist_frame1, hist_frame2, canvas, canvas2, ax1, ax2

def create_canvas_image(app, column, row):
    """
    Create a canvas and image object to display the processed image

    :param app (tk.Frame): The parent frame
    :param column (int): The column to place the canvas in
    :param row (int): The row to place the canvas in

    :return: The canvas and image object
    """
    canvas_img = tk.Canvas(app, width=RESIZED_VIDEO_WIDTH, height=RESIZED_VIDEO_HEIGHT, bg="white")
    canvas_img.grid(row=row, column=column, padx=5, pady=5)
    canvas_img_img = canvas_img.create_image(0, 0, anchor="nw")
    return canvas_img, canvas_img_img

def button_click():
    """
    Function to be called when the button is clicked
    """
    global elements_hidden
    print("Button clicked")

    if elements_hidden:
        canvas_img2.grid(row=1, column=2, padx=5, pady=5)
        hist_frame_2.grid(row=2, column=2, padx=5, pady=5)
    else:
        canvas_img2.grid_remove()
        hist_frame_2.grid_remove()

    elements_hidden = not elements_hidden

def create_gui(root):
    """
    Create the GUI
    
    :param root: The root of the GUI
    """
    app = tk.Frame(root, bg="white")
    app.grid(sticky="nsew")
    title_label = create_title_label(app, "AGV DEMO")
    lmain = create_video_label(app)
    hist_frame1, hist_frame_2, canvas, canvas2, ax1, ax2 = create_histogram_frames(app)

    canvas_img1, canvas_img1_img = create_canvas_image(app, 0, 1)
    canvas_img2, canvas_img2_img = create_canvas_image(app, 2, 1)

    # Create timer label
    timer_label = create_timer_label(app)
    # Create button
    button = create_button(app,"single/multi player")

    return app,title_label, lmain, hist_frame1, hist_frame_2, canvas, canvas2, ax1, ax2, canvas_img1, canvas_img2, canvas_img1_img, canvas_img2_img, timer_label

def main():
    global root, lmain, cap, data1, data2, ax1, ax2, canvas, canvas2, canvas_img1, canvas_img1_img, canvas_img2, canvas_img2_img, timer_label, hist_frame_2, elements_hidden
    elements_hidden = False


    root = tk.Tk()
    root.geometry(RESOLUTION)

    data1 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255
    data2 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255

    app, title_label, lmain, hist_frame1, hist_frame_2, canvas, canvas2, ax1, ax2, canvas_img1, canvas_img2, canvas_img1_img, canvas_img2_img, timer_label = create_gui(root)

    cap = cv2.VideoCapture(INPUT_VIDEO)
    
    
    end_time = time.time() + 30
    update_timer(timer_label, end_time)
    
    video_stream()

    root.mainloop()


if __name__ == "__main__":
    WIDTH = 1920
    HEIGHT = 1080
    RESOLUTION = f"{WIDTH}x{HEIGHT}"
    
    RESIZED_VIDEO_WIDTH = 600
    RESIZED_VIDEO_HEIGHT = 400
    
    RESIZED_VIDEO_SIZE = (RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT)
    
    INPUT_VIDEO = "footage/hall_cut.avi"
    main()