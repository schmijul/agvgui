#!/usr/bin/python3

import random
import socket
import tkinter as tk
import cv2
import numpy as np
import time



from PIL import ImageTk, Image
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

# own modules
from agvpresenter import AgvPresenter
from gui_fcts import *

# Function to receive UDP packets
def receive_udp_packets():
    # Receive data from AGV 1
    try:
        raw_data_1, _ = udp_source_agv_1.recvfrom( 64 )
        if raw_data_1:
            ### TODO: richtiges zuordnen der empfangenen daten
            power_dB_agv_1 = int.from_bytes( raw_data_1[0:4], byteorder='little', signed=True )
            
            data1[:, :, 0] = power_dB_agv_1
            print( "Power- AGV 1: %d dB" % power_dB_agv_1 )
    except BlockingIOError:
        pass
    
    # Receive data from AGV 2
    try:
        raw_data_2, _ = udp_source_agv_2.recvfrom( 64 )
        if raw_data_2:
            ### TODO: richtiges zuordnen der empfangenen daten
            power_dB_agv_2 = int.from_bytes( raw_data_2[0:4], byteorder='little', signed=True )
            data2[:, :, 0] = power_dB_agv_2
            print( "Power- AGV 2: %d dB" % power_dB_agv_2 )
    except BlockingIOError:
        pass
    
    
    
   
    
    # call this function again after idle time
    root.after_idle( 1, receive_udp_packets )

def video_stream():
    global symbol_added # True if symbol is added to frame
    global symbol_add_time # Time when symbol was added to frame
    global added_image # symbol to be added to frame
    global symbol_x # x coordinate of symbol
    global symbol_y # y coordinate of symbol
    global symbol_h # height of symbol in px
    global symbol_w # width of symbol in px
    global data1 # power data and associated coordinates for AGV 1
    global data2 # power data and associated coordinates for AGV 2

    _, frame = cap.read()
    if frame is None:
        root.quit() # end program if no frame is read
    else:
        if not symbol_added and time.time() - symbol_add_time >= 10: # add symbol every 10 seconds if no symbol is present
            random_filename = random.choice(list(images_dict.keys()))
            added_image = images_dict[random_filename]
            # resize added_image to 10x10 pixels
            added_image = cv2.resize(added_image, (40, 40))
            
            symbol_h, symbol_w = added_image.shape[0], added_image.shape[1]
            frame_h, frame_w, _ = frame.shape
            
            # Generate random coordinates for symbol
            symbol_x = random.randint(0, frame_w - symbol_w)
            symbol_y = random.randint(0, frame_h - symbol_h)

            # Check if symbol's coordinates already present in data1 
            symbol_coordinates = [symbol_x, symbol_y, symbol_w, symbol_h]
            if not (symbol_coordinates[0] in data1 and symbol_coordinates[1] in data1 and
                    symbol_coordinates[0] + symbol_coordinates[2] in data1 and symbol_coordinates[1] + symbol_coordinates[3] in data1): # add symbol if coordinates are not in data1
                frame[symbol_y:symbol_y + symbol_h, symbol_x:symbol_x + symbol_w] = added_image
                symbol_added = True
                print("added symbol")

        if symbol_added:
            frame[symbol_y:symbol_y + symbol_h, symbol_x:symbol_x + symbol_w] = added_image # add symbol to frame

            # Check if symbol's coordinates are in data1
            symbol_coordinates = [symbol_x, symbol_y, symbol_w, symbol_h]
            if (symbol_coordinates[0] in data1 and symbol_coordinates[1] in data1 and
                symbol_coordinates[0] + symbol_coordinates[2] in data1 and symbol_coordinates[1] + symbol_coordinates[3] in data1): # remove symbol if coordinates are in data1
                symbol_added = False # remove symbol if coordinates are in data1
                print("removed symbol")

        imgtk = process_frame(frame, resized_resolution=RESIZED_VIDEO_SIZE) # process frame
        lmain.imgtk = imgtk # update image in label
        lmain.configure(image=imgtk)

        global last_hist_update_time
        current_time = time.time()
        if current_time - last_hist_update_time >= 1.0: # update histogram every second
            last_hist_update_time = current_time
            
            agv_presenter1.update_histogram(data1)
            agv_presenter2.update_histogram(data2)

            agv_presenter1.update_canvas_image(data1)
            agv_presenter2.update_canvas_image(data2)

            root.after(1, agv_presenter1.hist_canvas.draw)
            root.after(1, agv_presenter2.hist_canvas.draw)
        lmain.after(1, video_stream)





        
def update_timer_label():
    global TIMER_LENGTH
    timer_label.config(text=f"Time remaining: {TIMER_LENGTH}s")
    TIMER_LENGTH -= 1
    if TIMER_LENGTH < 0:
        
        
        if len(data1) > len(data2):
            print("AGV 1 hat gewonnen")
        elif len(data1) < len(data2):
            print("AGV 2 hat gewonnen")
        else:
            print("Unentschieden")
    root.after(1000, update_timer_label)


def create_gui(root):
    app = tk.Frame(root, bg="#c0c0c0")
    app.grid(sticky="nsew")
    title_label = create_title_label(app, "AGV DEMO")
    lmain = create_video_label(app)

    agv_presenter1 = AgvPresenter(app, column=1, row=1, width=RESIZED_VIDEO_WIDTH, height=RESIZED_VIDEO_HEIGHT)
    agv_presenter2 = AgvPresenter(app, column=3, row=1, width=RESIZED_VIDEO_WIDTH, height=RESIZED_VIDEO_HEIGHT)

    sidebar = create_sidebar(app, column=0, row=0, width=100)

    timer_label = tk.Label(app, text=f"Time remaining: {TIMER_LENGTH}s", font=("Arial", 16), bg="white")
    timer_label.grid(row=3, column=2, pady=(10, 0))
    
    
    result_label = tk.Label(app, font=("Arial", 16), bg="white")
    result_label.grid(row=4, column=2, pady=(10, 0))

    return app, title_label, lmain, agv_presenter1, agv_presenter2, sidebar,timer_label, result_label




def create_sidebar(app, column, row, width):
    sidebar = tk.Frame(app, width=width // 6, bg="#D3D3D3")
    sidebar.grid(row=row, column=column, rowspan=3, padx=5, pady=5, sticky="nsew")

    spacer_label = tk.Label(sidebar, text="AGV DEMO", font=("Arial", 18), bg="#232323", fg="white")
    spacer_label.pack(pady=(10, 10), padx=(5, 5))

    # create "Toggle Player 2" button
    toggle_button = tk.Button(sidebar, text="Toggle Player 2", command=toggle_player2, bg="#232323", fg="white")
    toggle_button.pack(pady=5, padx=(5, 5))

    # create "New Game" button
    new_game_button = tk.Button(sidebar, text="New Game", command=reset, bg="#232323", fg="white")
    new_game_button.pack(pady=5, padx=(5, 5))

    return sidebar

def reset():
    global TIMER_LENGTH, data1, data2
    TIMER_LENGTH = 30 # reset timer to 30 seconds
    data1 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255 # reset data1 
    data2 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255 # reset data2 to all zeros
    result_label.config(text="") # clear the result label

  
def update_result_label():
    if len(data1) > len(data2):
        result_label.config(text="AGV 1 hat gewonnen")
    elif len(data1) < len(data2):
        result_label.config(text="AGV 2 hat gewonnen")
    else:
        result_label.config(text="Unentschieden")
    root.after(1000, update_result_label)
    
    
def toggle_player2():
    
    global player2_visible
    if player2_visible:
        # remove player 2 from GUI
        agv_presenter2.canvas_img.grid_remove() 
        agv_presenter2.hist_frame.grid_remove() 
        player2_visible = False
    else:
        # add player 2 to GUI
        agv_presenter2.canvas_img.grid(row=1, column=3, padx=5, pady=5)
        agv_presenter2.hist_frame.grid(row=2, column=3, padx=5, pady=5)
        player2_visible = True
        
   
def main():
    global root, lmain, cap, data1, data2, agv_presenter1, agv_presenter2,timer_label, player2_visible,images_dict, udp_source_agv_1,udp_source_agv_2, player2_visible, data1, data2, result_label
    images_dict = load_images_from_folder() # load power up symbols from folder

    player2_visible = True
    global last_hist_update_time
    last_hist_update_time = time.time()

    root = tk.Tk()
    root.geometry(RESOLUTION)

    data1 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255
    data2 = np.ones((RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT, 3)) * 255

    app, title_label, lmain, agv_presenter1, agv_presenter2, sidebar, timer_label,result_label = create_gui(root)
    
    
    # socket for receiving data from AGVs
    udp_source_agv_1 = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
    udp_source_agv_1.bind( ("0.0.0.0", RECEIVE_PORT_AGV_1 ) )
    udp_source_agv_1.setblocking(False)
    
    # socket for receiving data from AGVs
    udp_source_agv_2 = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
    udp_source_agv_2.bind( ("0.0.0.0", RECEIVE_PORT_AGV_2 ) )
    udp_source_agv_2.setblocking(False)
    
    
    
    # for debugging : if True, the video stream is received via gstreamer otherwise via webcam
    
    if STREAM:
        gst_pipeline = (
        "udpsrc port=1234 ! application/x-rtp,media=video,clock-rate=90000,encoding-name=VP8-DRAFT-IETF-01 ! "
        "rtpvp8depay ! vp8dec ! videoconvert ! appsink"
        )

        cap = cv2.VideoCapture(gst_pipeline, cv2.CAP_GSTREAMER)
    else:
        cap = cv2.VideoCapture(0)

    global symbol_add_time, symbol_added, symbol_x,symbol_y,symbol_h,symbol_w, added_image
    added_image = None

    symbol_add_time = time.time()
    symbol_added = False
    symbol_x = None
    symbol_y = None
    symbol_h = None
    symbol_w = None

    video_stream()
    receive_udp_packets()
    update_timer_label()
    update_result_label()
    root.mainloop()


if __name__ == '__main__':
    WIDTH = 1920
    HEIGHT = 1080
    RESOLUTION = f"{WIDTH}x{HEIGHT}"

    RESIZED_VIDEO_WIDTH = 600
    RESIZED_VIDEO_HEIGHT = 400

    RESIZED_VIDEO_SIZE = (RESIZED_VIDEO_WIDTH, RESIZED_VIDEO_HEIGHT)

    STREAM = True
    TIMER_LENGTH = 30
    
    RECEIVE_PORT_AGV_1 = 3751
    RECEIVE_PORT_AGV_2 = 3752


    main()
    
 