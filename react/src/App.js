import React, { useState, useRef, useEffect } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import './App.css';

function App() {
  const videoRef = useRef(null);
  const [playing, setPlaying] = useState(false);
  const [data1, setData1] = useState(Array(100).fill({name: 0, value: 0}));
  const [data2, setData2] = useState(Array(100).fill({name: 0, value: 0}));
  const [timer, setTimer] = useState(60);

  const [canvas1, setCanvas1] = useState(null);
  const [canvas2, setCanvas2] = useState(null);

  const togglePlay = () => {
    if (playing) {
      videoRef.current.pause();
    } else {
      videoRef.current.play();
    }

    setPlaying(!playing);
  };

  useEffect(() => {
    if (!playing || timer <= 0) return;

    const interval = setInterval(() => {
      setTimer(prevTimer => prevTimer - 1);
    }, 1000);

    return () => clearInterval(interval);
  }, [playing, timer]);

  useEffect(() => {
    const interval = setInterval(() => {
      setData1(prevData => {
        const newData = [...prevData];
        for (let i = 0; i < 10; i++) {
          const index = Math.floor(Math.random() * newData.length);
          newData[index] = { name: index, value: Math.random() };
        }
        return newData;
      });

      setData2(prevData => {
        const newData = [...prevData];
        for (let i = 0; i < 10; i++) {
          const index = Math.floor(Math.random() * newData.length);
          newData[index] = { name: index, value: Math.random() };
        }
        return newData;
      });

      // Draw random pixels on canvas1
      if (canvas1) {
        const context = canvas1.getContext('2d');
        for (let i = 0; i < 10; i++) {
          const x = Math.floor(Math.random() * canvas1.width);
          const y = Math.floor(Math.random() * canvas1.height);
          const r = Math.floor(Math.random() * 255);
          const g = Math.floor(Math.random() * 255);
          const b = Math.floor(Math.random() * 255);
          context.fillStyle = `rgb(${r}, ${g}, ${b})`;
          context.fillRect(x, y, 1, 1);
        }
      }

      // Draw random pixels on canvas2
      if (canvas2) {
        const context = canvas2.getContext('2d');
        for (let i = 0; i < 10; i++) {
          const x = Math.floor(Math.random() * canvas2.width);
          const y = Math.floor(Math.random() * canvas2.height);
          const r = Math.floor(Math.random() * 255);
          const g = Math.floor(Math.random() * 255);
          const b = Math.floor(Math.random() * 255);
          context.fillStyle = `rgb(${r}, ${g}, ${b})`;
          context.fillRect(x, y, 1, 1);
        }
      }
    }, 1000);
    return () => clearInterval(interval);
  }, [canvas1, canvas2]);

 
  return (
    <div className="App">
      <div className="canvas-row">
        <div className="canvas-container">
          <canvas ref={setCanvas1} width={400} height={400} />
        </div>
        <div className="video-container">
        <h1>AGV DEMO</h1>
          <video
            ref={videoRef}
            width="600"
            height="400"
            src={process.env.PUBLIC_URL + '/hall_cut.mp4'}
          />
        </div>
        <div className="canvas-container">
          <canvas ref={setCanvas2} width={400} height={400} />
        </div>
      </div>
      <div className="charts-container">
        <div className="chart-container">
          <BarChart width={400} height={200} data={data1}>
            <XAxis dataKey="name" />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend />
            <Bar dataKey="value" fill="#8884d8" />
          </BarChart>
        </div>
        <div className="timer-container">
          Timer: {Math.floor(timer / 60)}:{(timer % 60).toString().padStart(2, '0')}
        </div>
        <div className="chart-container">
          <BarChart width={400} height={200} data={data2}>
            <XAxis dataKey="name" />
            <YAxis domain={[0, 1]} />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend />
            <Bar dataKey="value" fill="#8884d8" />
          </BarChart>
        </div>
      </div>
      <div className="container">
        <button onClick={togglePlay}>{playing ? 'Pause' : 'Play'}</button>
      </div>
    </div>
  );
}

export default App;
